#!/bin/bash
#
# Create pipenv Pipfile from converting Anaconda environment.yml

# shellcheck disable=SC1117
python3 <<EOF
import os
from pprint import pformat

import yaml

with open('environment.yml', 'r') as stream:
    try:
        env = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
packages_orig = [package
                 for package in env['dependencies']]
# Remove pinned versions, because they resolve in conda but conflict in pip.
# Also filter out conda packages and python version.
packages =  [package.split('=')[0]
             for package in packages_orig
             if package.split('=')[0] != 'python' and
                package.find('conda') == -1 and
                package.find('jupyter_') == -1]
print('conda packages:\n', pformat(packages_orig),
      '\npip packages:\n', pformat(packages))
with open('requirements.txt', 'w') as stream:
    stream.writelines([package + os.linesep
                       for package in packages])
EOF

# Remove any old Pipefile created from this script.
rm -f Pipfile
# This reads our above, newly created requirements.txt file and creates the
# Pipenv file.
pipenv --three

# Install packages from our newly created Pipenv file.
# Using the lock file takes several minutes and is 36 kb!  Skip it.
pipenv install --skip-lock
